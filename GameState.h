#pragma once
#include <SDL.h>

class Game;
class GameState
{
public:
	GameState(void);
	virtual ~GameState(){return;}
	virtual void draw(SDL_Window *window, Game& context)=0;
	virtual void init(Game& context)=0;
	virtual bool handleSDLEvent(SDL_Event const &sdlEvent, Game& context)=0;
protected:
	friend class Game;
	void changeState(GameState* state, Game& context);
	void update(Game& context);

};

