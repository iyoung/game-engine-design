#include "StatePlay.h"
#include "Game.h"

StatePlay::StatePlay(void)
{
}
void StatePlay::init(Game& context)
{
	label = new GameLabel();
	xpos = 0.0f;
	ypos = 0.0f;
	xsize = 0.15f;
	ysize = 0.15f;

	targetXPos = 0.0f;
	targetYPos = 0.0f;
	targetXSize = 0.1f;
	targetYSize = 0.1f;

	score = 0;

	glClearColor(0.0, 0.0, 0.0, 0.0); // set background colour
	std::srand( std::time(NULL) );
	targetXPos = (float)rand()/RAND_MAX - 0.75f;
	targetYPos = (float)rand()/RAND_MAX - 0.75f;
	lastTime = clock();
	window = setUpRC(context);
	context.setRC(window);
}
SDL_Window* StatePlay::setUpRC(Game& context)
{
	 if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        Game::ExitFatalError("Unable to initialize SDL"); 

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        Game::ExitFatalError("Unable to create window");
 
    glContext = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	return window;
}
bool StatePlay::handleSDLEvent(SDL_Event const &sdlEvent, Game& context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		if (sdlEvent.type == SDL_KEYDOWN)
		{
			//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
			//std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
			switch( sdlEvent.key.keysym.sym )
			{
			case SDLK_UP:
				case 'w': case 'W': 
					ypos += 0.05f;
					break;
				case SDLK_DOWN:
				case 's': case 'S':
					ypos -= 0.05f;
					break;
				case SDLK_LEFT:
				case 'a': case 'A': 
					xpos -= 0.05f;
					break;
				case SDLK_RIGHT:
				case 'd': case 'D':
					xpos += 0.05f;
					break;
				case SDLK_ESCAPE:
					changeState(context.getMenuState(),context);
				default:
					break;
			}

		if ( sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			changeState(context.getMenuState(), context);
		}
	}
	return true;
}
void StatePlay::draw(SDL_Window* window, Game& context)
{
	update();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	//Show Score and frame time information
	std::stringstream strStream;
    strStream << "Score: " << score;
    strStream << "          ms/frame: " << milliSecondsPerFrame;
	strStream << "			FPS: "<< fps;
	//generate and draw texture for score & time info
	label->textToTexture(strStream.str().c_str());
	label->draw(-0.9f,0.9f);
	// draw player
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
	  glVertex3f (xpos, ypos, 0.0); // first corner
	  glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	  glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	  glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();
	//draw player texture
  label->textToTexture("Player");
  label->draw(xpos+(xsize/2.0f), ypos+ysize);
    // draw target
    glColor3f(1.0,0.0,0.0);
    glBegin(GL_POLYGON);
      glVertex3f (targetXPos, targetYPos, 0.0); // first corner
      glVertex3f (targetXPos+targetXSize, targetYPos, 0.0); // second corner
      glVertex3f (targetXPos+targetXSize, targetYPos+targetYSize, 0.0); // third corner
      glVertex3f (targetXPos, targetYPos+targetYSize, 0.0); // fourth corner
    glEnd();
	//draw target texture
	label->textToTexture("Target");
	label->draw(targetXPos+(targetXSize/2.0f),  targetYPos+targetYSize);
	//SDL_GL_SwapWindow(window); // swap buffers
	SDL_GL_SwapWindow(window); // swap buffers
}
void StatePlay::update()
{
	if ( (targetXPos >= xpos) && (targetXPos+targetXSize <= xpos+xsize)	// cursor surrounds target in x
	  && (targetYPos >= ypos) && (targetYPos+targetYSize <= ypos+ysize) ) // cursor surrounds target in y
    {
		score += 100; // congrats, player has scored!
		// randomize the new target position
		targetXPos = (float)rand()/RAND_MAX - 0.75f;
		targetYPos = (float)rand()/RAND_MAX - 0.75f;
    }

    // Calculate ms/frame
    // Some OpenGL drivers will limit the frames to 60fps (16.66 ms/frame)
    // If so, expect to see the time to rapidly switch between 16 and 17...
	glColor3f(1.0,1.0,1.0);
    currentTime = clock();
    // On some systems, CLOCKS_PER_SECOND is 1000, which makes the arithmetic below redundant
    // - but this is not necessarily the case on all systems
    milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);
	fps = (float)(1000)/(currentTime-lastTime);
    lastTime = clock();
}
StatePlay::~StatePlay(void)
{
	delete label;
}


