#pragma once
#include "gamestate.h"
#include "SDL.h"
#include "SDL_ttf.h"
#include <GL/glew.h>
#include "GameLabel.h"
#include <cstdlib>
#include <ctime>
// iostream for cin and cout
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>

class StatePlay :
	public GameState
{
public:
	StatePlay(void);
	virtual void draw(SDL_Window* window, Game& context);
	virtual void init(Game& context);
	virtual bool handleSDLEvent(SDL_Event const &sdlEvent, Game& context);
	~StatePlay(void);
private:
	void update();
	SDL_Window* setUpRC(Game& context);
	friend class Game;
	float xpos;
	float ypos;
	float xsize;
	float ysize;
	float targetXPos;
	float targetYPos;
	float targetXSize;
	float targetYSize;
	int score;
	float milliSecondsPerFrame;
	float fps;
	GameLabel *label;
	SDL_Window *window; //create window for drawing in
	SDL_GLContext glContext; // OpenGL context handle
	TTF_Font* textFont;	// SDL type for True-Type font rendering
	clock_t lastTime; // clock_t is an integer type
	clock_t currentTime; // use this to track time between frames
	friend class Game;
};

