#pragma once

#include <GL\glew.h>


class GameLabel
{
public:
	GameLabel(void);
	void textToTexture(const char* str);
	void draw(float x, float y);
	~GameLabel(void);
private:
	void ExitFatalError(char* message);
	GLuint texID;
	GLuint height;
	GLuint width;

};

