#pragma once
#include "gamestate.h"
class StateMainMenu :
	public GameState
{
public:
	virtual void draw(Game &game);
	virtual void handleEvent(SDL_Event const &sdlEvent, Game &game);
	StateMainMenu(void);
	~StateMainMenu(void);
};

