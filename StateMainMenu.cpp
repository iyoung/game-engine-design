#include "StateMainMenu.h"


StateMainMenu::StateMainMenu(void)
{
	label = new GameLabel();
	label->textToTexture("Menu Open. Press escape to go back to game");
}
void StateMainMenu::draw(Game& game)
{
	Update(game);
	SDL_Window *window = Draw(game);
	label->draw(-0.8f,-0.8f);
	SDL_GL_SwapWindow(window); // swap buffers
}
void StateMainMenu::handleEvent(SDL_Event const &sdlEvent, Game& game)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if( sdlEvent.key.keysym.sym == SDLK_ESCAPE)
			changeState(game,game.getMenuState());
		else
			return;
	}
}
StateMainMenu::~StateMainMenu(void)
{
}
