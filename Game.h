#pragma once
#include "SDL.h"
#include "SDL_ttf.h"
#include <GL/glew.h>
#include "GameLabel.h"
#include "GameState.h"
#include "StatePlay.h"
#include "MenuState.h"
// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>
// iostream for cin and cout
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>

class Game
{
public:
	Game(void);
	void init();
	void Run();
	static void ExitFatalError(char *message);
	GameState* getPlayState(){return playState;}
	GameState* getMenuState(){return menuState;}
	void setRC(SDL_Window* win){window=win;}
	SDL_Window* getRC(){return window;}
	void Draw();
	~Game(void);
private:
	void SetupRC();
	void setState(GameState* state){currentState=state;}
	//member variables
	SDL_Window* window;
	bool HandleSDLEvent(SDL_Event const &sdlEvent, bool running);
	//member variables
	friend class GameState;
	friend class PlayState;
	friend class MenuState;
	GameState* currentState;
	GameState* menuState;
	GameState* playState;
};

