#pragma once
#include "gamestate.h"
#include "GameLabel.h"
class MenuState :
	public GameState
{
public:
	MenuState(void);
	virtual void draw(SDL_Window* window, Game& context);
	virtual void init(Game& context);
	virtual bool handleSDLEvent(SDL_Event const &sdlEvent, Game& context);
	~MenuState(void);
private:
	friend class Game;
	GameLabel* label;
};

