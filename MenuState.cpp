#include "MenuState.h"
#include "Game.h"

MenuState::MenuState(void)
{

}
void MenuState::init(Game& context)
{
	label = new GameLabel();
	label = new GameLabel();
}
void MenuState::init(Game& context)
{
	label->textToTexture("Menu Open");
}
bool MenuState::handleSDLEvent(SDL_Event const &sdlEvent, Game& context)
{
	bool temp = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		if ( sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			changeState(context.getPlayState(), context);
			temp = true;
		}
		if (  sdlEvent.key.keysym.sym == SDLK_SPACE)
		{
			temp = false;
			SDL_DestroyWindow(context.getRC());
		}
	}
	return temp;
}

void MenuState::draw(SDL_Window* window, Game& context)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture("Menu Open");
	label->draw(-0.4,0.8);
	label->textToTexture("Esc: return to game");
	label->draw(-0.4,0.6);
	label->textToTexture("Space: Quit Game");
	label->draw(-0.4,0.4);
	SDL_GL_SwapWindow(window); // swap buffers
	
}
MenuState::~MenuState(void)
{
	delete label;
	label->textToTexture("Menu Open");
}
