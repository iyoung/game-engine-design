#include "Game.h"


Game::Game(void)
{
	window = NULL;
	playState = new StatePlay();
	menuState = new MenuState();
	currentState = playState;
	playState->init(*this);
	menuState->init(*this);
}
void Game::init()
{
	playState->init(*this);
	menuState->init(*this);
	playState = new StatePlay();
	menuState = new MenuState();
	currentState = playState;
	currentState->init(*this);
}
void Game::SetupRC()
{
	 if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        ExitFatalError("Unable to initialize SDL"); 

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        ExitFatalError("Unable to create window");
 
    glContext = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	
}
void Game::Run(void) 
{
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	std::cout << "Progress: About to enter main loop" << std::endl;

	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				running = currentState->handleSDLEvent(sdlEvent,*this);
		}
		currentState->draw(window, *this); // this is the place to put a call to the game update function
	}
	SDL_DestroyWindow(window);
	SDL_Quit();
    
}


void Game::ExitFatalError(char *message)
{
	std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}
Game::~Game(void)
{
	delete playState;
	delete menuState;
}
